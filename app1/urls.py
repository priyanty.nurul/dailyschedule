from django.contrib import admin
from django.urls import include, path

# schedule_view, history_view
from .views import index, schedule_detail_view, schedule_create_view

app_name = 'app1'

urlpatterns = [
    path('', index, name='index'),
    path('arrangement/', schedule_create_view, name='arrange'),
    #path('schedule/', schedule_view, name='check'),
    #path('history/', history_view, name='history'),
    path('detail/<int:schedule_id>', schedule_detail_view, name='detail'),
]
