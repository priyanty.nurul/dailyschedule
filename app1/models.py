from django.db import models

# Create your models here.


class Schedule(models.Model):
    mata_kuliah = models.CharField(max_length=40, blank=False)
    dosen_pengajar = models.CharField(max_length=40, blank=False)
    jumlah_sks = models.PositiveSmallIntegerField(blank=False)
    semester_tahun = models.CharField(max_length=15, blank=False)
    deskripsi = models.TextField(blank=True)
