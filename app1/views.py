from django.shortcuts import render
"""import datetime
"""
from .forms import ScheduleForm

from .models import Schedule
# Create your views here.


def index(request):
    schedules = Schedule.objects.all()
    context = {
        'nama': 'Nurul',
        'schedules': schedules
    }
    return render(request, 'app1/index.html', context)


def schedule_create_view(request):
    form = ScheduleForm(request.POST or None)
    if form.is_valid():
        form.save()
        # to re-render form after submission (clears the form, so we know it's already submitted)
        form = ScheduleForm()

    context = {
        'form': form,
    }
    return render(request, 'app1/schedule_arrangement.html', context)


def schedule_detail_view(request, schedule_id):
    schedule = Schedule.objects.get(id=schedule_id)
    context = {
        'schedule': schedule,
    }
    return render(request, 'app1/schedule_detail.html', context)
