from django.test import TestCase, Client
from django.urls import resolve

from .views import index, schedule_create_view, schedule_detail_view, schedule_view
from .models import Schedule
# Create your tests here.


class CategoryTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_arrangement_url_is_exist(self):
        response = Client().get('/arrangement/')
        self.assertEqual(response.status_code, 200)

    def test_schedule_url_is_exist(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_schedule_detail_url_is_exist(self):
        response = Client().get('/detail/')
        self.assertEqual(response.status_code, 200)

    def test_category_using_the_right_func(self):
        category_response = Client().get('//')
        self.assertEqual(category_response.resolver_match.func, index)

    def test_category_using_index_template(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_can_create_new_kategori(self):
        # Creating a new category
        kategori = Schedule.objects.create(
            mata_kuliah='PPW', dosen='Meganingrum Aristajiwanggi', jumlah_sks=3, semester_tahun='Genap 2019/2020')

        # Retrieving all available category
        counting_all_available_activity = Schedule.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
