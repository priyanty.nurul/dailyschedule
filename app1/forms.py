from django import forms

from .models import Schedule


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = [
            'mata_kuliah',
            'dosen_pengajar',
            'jumlah_sks',
            'semester_tahun',
            'deskripsi'
        ]
