from django import forms
from .models import Kegiatan, Peserta


class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama_peserta']


class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
