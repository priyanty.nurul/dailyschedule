from django.db import models

# Create your models here.


class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=50, blank=False)
    tempat = models.CharField(max_length=20)

    def __str__(self):
        return self.nama_kegiatan


class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama_peserta = models.CharField(max_length=25, blank=False)

    def __str__(self):
        return self.nama_peserta
