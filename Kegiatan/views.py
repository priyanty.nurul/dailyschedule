from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm

# Create your views here.


def kegiatan(request):
    kegiatans = Kegiatan.objects.all()
    pesertas = Peserta.objects.all()
    context = {
        'kegiatans': kegiatans,
        'pesertas': pesertas,
    }
    return render(request, 'Kegiatan/kegiatan.html', context)
