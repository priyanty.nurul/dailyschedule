# re_path is for path that using regex (if used)
from django.urls import path, re_path
from . import views

app_name = 'Kegiatan'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    #path('add/', views.add_kegiatan, name='add-kegiatan'),
]
